package com.bawei.lixueyan.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.bawei.lixueyan.entity.Hello;

@Controller
public class HelloController {
	
	@RequestMapping("he")
	public String hellow() {
		Hello hello = new Hello();
		hello.name();
		return "hello";
	}

}
