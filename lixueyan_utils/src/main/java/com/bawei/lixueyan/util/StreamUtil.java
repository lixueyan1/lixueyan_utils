package com.bawei.lixueyan.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * 流工具类
 *  
 * @author lixueyan
 * @todo  TODO
 * @date  2021-1-16
 * @type_name  StreamUtil
 */
public class StreamUtil {
	
	// 1.批量关闭流
		public static void closings(AutoCloseable... closeables) throws Exception {
			// 非空判断
			if (closeables!=null) {
				for (AutoCloseable autoCloseable : closeables) {
					if (autoCloseable!=null) { // 逻辑更严谨
						autoCloseable.close();
					}
				}
			}
		}
		
		//  2.1流处理(自定义流是否关闭)
	    public static void copy(InputStream inputStream,  OutputStream outputStream, boolean closeInputStream, boolean closeOutputStream) throws  Exception {
	         // 用于复制的字节数组
	         byte[] b = new byte[1024];
	         int len = 0;
	         // 复制，如果讀取到的數據 >0 ,證明讀取到了數據
	         while ((len = inputStream.read(b)) > 0) {
	              // 将数据写入输出流
	              outputStream.write(b, 0, len);
	         }
	         // 关闭流
	         if (closeInputStream) {
	              closings(inputStream);
	         }
	         if (closeOutputStream) {
	              closings(outputStream);
	         }
	     }
	    
	    // 2.2 默认不关闭
	    public static void copy(InputStream inputStream,  OutputStream outputStream) throws Exception {
	         copy(inputStream, outputStream, false, false);
	     }
	    
	    // 3.读取文本流（自定义编码格式和是否关闭流）
	    public static String asString(InputStream inputStream,  String encode, boolean isClose) throws Exception {
	         // 存放结果
	         StringBuffer sb = new StringBuffer();
	         // 用于复制的字节数组
	         byte[] b = new byte[1024];
	         int len = 0;
	         // 复制，如果读取的个数 > 0，证明读取到了数据
	         while ((len = inputStream.read(b)) > 0) {
	              // 将数据写入StringBuffer
	              sb.append(new String(b, 0, len, encode));
	         }
	         // 关流
	         if (isClose) {
	              closings(inputStream);
	         }
	         return sb.toString();
	     }
		
	    // 3.1 自定义是否关闭流（默认编码格式）
	    public static String asString(InputStream inputStream,  boolean isClose) throws Exception {
	         return asString(inputStream, "UTF-8", true);
	     }
	    
	    // 3.2 自定义是否关闭流（默认关不关流）
	    public static String asString(InputStream inputStream) throws Exception {
	         return asString(inputStream, "UTF-8", true);
	     }
	    
	    // 4.读取文本文件（传入参数file）
	    public static String asString(File textFile) throws  Exception {
	         return asString(new  FileInputStream(textFile));
	     }
	    
	    // 4.1 默认编码格式，自定义流处理
	    public static String asString(File textFile, boolean  isClose) throws Exception {
	         return asString(new FileInputStream(textFile),  isClose);
	     }
	    
	    // 4.2 指定编码格式，自定义流处理
	    public static String asString(File textFile, String  encode, boolean isClose) throws Exception {
	        return asString(new FileInputStream(textFile),  encode, isClose);
	    }
	    
	    /**
	     * 	5.按行读取文本文件（返回List集合）;自定义编码格式
	     * @param textFile
	     * @param encode
	     * @return
	     * @throws Exception
	     */
	    public static List<String> readingLineFormTextFile(File  textFile, String encode) throws Exception {
	         // 存放结果
	         List<String> list = new ArrayList<String>();
	         // 字符输入流
	         FileReader fr = new FileReader(textFile);
	         // 缓冲字符输入流
	         BufferedReader br = new BufferedReader(fr);
	         // 用于存入复制的数据
	         String str = null;
	         // 复制，如果读取的不为空，证明读取到了数据
	         while ((str = br.readLine()) != null) {
	              list.add(str);
	         }
	         // 关流
	         closings(br);
	         closings(fr);
	         return list;
	     }
	    
	    /**
	     * 	5.1 默认编码格式
	     * @param textFile
	     * @return
	     * @throws Exception
	     */
	    public static List<String> readingLineFormTextFile(File  textFile) throws Exception {
	        return readingLineFormTextFile(textFile,"UTF-8");
	    }
	    
	    

}
