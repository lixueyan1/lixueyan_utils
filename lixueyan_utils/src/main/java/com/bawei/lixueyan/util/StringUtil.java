package com.bawei.lixueyan.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil {
	//字母正则
	private static final String LETTER_REGEX = "[a-zA-Z]+";
	//手机号正则
	private static final String PHONE_REGEX = "^1[3|4|5|7|8]\\d{9}$"; 	
	//邮箱正则
	private static final String EMAIL_REGEX1 = "^([a-z0-9A-Z]+[-|_|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
	

	/**
	 * 1. 判断传入的字符串是否为空(不包含空白字符)
	 * 
	 * @Date 2021-1-13
	 * @author lixueyan
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean hasText(String str) {
		// trim 字符串去空
		if (str.trim() != null && str.trim().length() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 2. 判断传入的字符串是否为空(包含空白字符)
	 * 
	 * @Date 2021-1-13
	 * @author lixueyan
	 * 
	 * @param str
	 * @return boolean
	 */
	public static boolean hasLength(String str) {
		if (str != null && str.length() > 0) {
			return true;
		}
		return false;
	}

	/**
	 * 3. 反转字符串
	 * 
	 * @Date 2021-1-13
	 * @author lixueyan
	 * 
	 * @param str
	 * @return String
	 */
	public static String reverse(String str) {
		// 判断字符串是否为空
		if (!hasText(str)) {
			return "字符串不能为空";
		}
		return new StringBuffer(str).reverse().toString();

	}

	/**
	 * 4.  隐藏字符串(过滤敏感信息)
	 * 
	 * @Date 2021-1-13
	 * @author lixueyan
	 * 
	 * @param str
	 * @param start
	 * @param end
	 * @return String
	 */
	public static String hiddenStr(String str, int start, int end) {

		// 判断字符串是否为空
		if (!hasText(str)) {
			return "字符串不能为空！";
		}
		if (end <= start) {
			return "初始值要小于结束值！";
		}
		String preStr = str.substring(0, start-1);
		String endStr = str.substring(end-1, str.length());
		for (int i = 0; i < end - start; i++) {
			preStr += "*";
		}
		String resultStr = preStr + endStr;
		return resultStr;
	}
	
	
	/**
	 * 5. 判断传入字符串是否为字母
	 * @Date 2021-1-13
	 * @author lixueyan
	 * @return boolean
	 * @param str
	 * @return
	 */
	public static boolean isLetter(String str) {
		// 判断字符串是否为空
		if (!hasText(str)) {
			return false;
		}
		
		return str.matches(LETTER_REGEX);

	}
	
	/**
	 * 6. 验证是否为合格手机号码
	 * @Date 2021-1-13
	 * @author lixueyan
	 * @return boolean
	 * @param src
	 * @return
	 */
	public static boolean isPhone(String src) {
		// 判断字符是否有值
		if (!hasText(src)) {
			return false;
		}
		
		return src.matches(PHONE_REGEX);
	}
	/**
	 * 
	 * @Description 7. 验证是否为合格邮箱 
	 * @Title StringUtil.java
	 * @Date 2021-1-13
	 * @author lixueyan
	 * @return boolean
	 * @param email
	 * @return
	 */
	public static boolean isEmail(String email) {
		// 判断字符是否有值
		if (!hasText(email)) {
			return false;
		}
			
		return email.matches(EMAIL_REGEX1);
	}
	
	/**
	 * 
	 * @Description 根据正则将字符串中数据取出
	 * @Title getPlaceholderValue1
	 * @Date 2021-1-13
	 * @author lixueyan
	 * @return String
	 * @param str
	 * @return
	 */
	public static boolean getPlaceholderValue(String str) {
		// 取出规则数据的正则表达式
		String regenx = "[0-9]+(?=[^0-9]*$)";
		// 判断字符串是否为空
		if (!hasText(str)) {
			return false;
		}

		// 编译规则
		Pattern p = Pattern.compile("[0-9]+(?=[^0-9]*$)");
		// Pattern 匹配src
		Matcher matcher = p.matcher(str);
		if (matcher.find()) {
			return true;
		}
		return false;
	}
	
	
	/**
	 * 根据正则将字符串A中数据取出来：例如在“http://news.cnstock.com/news,yw-201908-4413224.htm”把“4413224”提取出来（）
	 * @Description 
	 * @Title getPlaceholderValue1
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return String
	 * @param str
	 * @return
	 *
	 */
	public static String getPlaceholderValue1(String str) {
		// 取出规则数据的正则表达式
		String regenx = "[0-9]+(?=[^0-9]*$)";
		// 判断字符串是否为空
		if (!hasText(str)) {
			return "传入参数不能为空！";
		}
		// 编译规则
		Pattern p = Pattern.compile("[0-9]+(?=[^0-9]*$)");
		// Pattern 匹配src
		Matcher matcher = p.matcher(str);
		if (matcher.find()) {
			return matcher.group();
		}
		return null;
	}

}
