package com.bawei.lixueyan.util;

import java.io.File;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * 文件工具类
 *  
 * @author lixueyan
 * @todo  TODO
 * @date  2021-1-14
 * @type_name  FileUtil
 */
public class FileUtil {
	/**
	 * 获取用户当前目录的路径
	 * @Description 
	 * @Title getUserDir
	 * @Date 2021-1-14
	 * @author lixueyan
	 * @return File
	 * @return
	 *
	 */
	 public static File getUserDir() {
         //获取用户当前目录的路径
         String path = System.getProperty("user.dir");
         return new File(path);
     }
	 /**
	  * 获取操作系统目录的路径
	  * @Description 
	  * @Title getUserHomeDir
	  * @Date 2021-1-14
	  * @author lixueyan
	  * @return File
	  * @return
	  *
	  */
	 public static File getUserHomeDir() {
         String path = System.getProperty("user.home");
         return new File(path);
     }
	 /**
	  * 获取操作系统临时目录
	  * @Description 
	  * @Title getTmpDir
	  * @Date 2021-1-14
	  * @author lixueyan
	  * @return File
	  * @return
	  *
	  */
	 public static File getTmpDir() {
         String path =  System.getProperty("java.io.tmpdir");
         return new File(path);
     }
	 /**
	  * 获取一个文件的根路径
	  * @Description 
	  * @Title getRoot
	  * @Date 2021-1-14
	  * @author lixueyan
	  * @return File
	  * @param file
	  * @return
	  *
	  */
	 public static File getFileRoot(File file) {
         //获取文件的路径
         String name = file.getPath();
         int indexOf = name.indexOf(":");
         //根据:进行截取
         String fileRoot = name.substring(0, indexOf + 1); 
         return new File(fileRoot);
     }
	 /**
	  * 获取系统根目录
	  * @Description 
	  * @Title getSystemRoot
	  * @Date 2021-1-14
	  * @author lixueyan
	  * @return File
	  * @return
	  *
	  */
	 public static File getSystemRoot() {
         File file = getUserHomeDir();
         return getFileRoot(file);
     }
	 /**
	  * 获取获取当前所在目录
	  * @Description 
	  * @Title getRoot
	  * @Date 2021-1-14
	  * @author lixueyan
	  * @return File
	  * @return
	  *
	  */
	 public static File getRoot() {
         //获取当前所在目录
         File userDir = getUserDir();
         //根据当前所在目录，获取其根目录
         return getFileRoot(userDir);
     }
	 /**
	  * 6.1 获取文件的扩展名(传入参数为file)
	  * @Description 
	  * @Title getFileExeName
	  * @Date 2021-1-16
	  * @author lixueyan
	  * @return String
	  * @param file
	  * @return
	  *
	  */
	 public static String getFileExeName(File file) {
			return getFileExeName(file.getName());
		}
	 /**
	  * 6.2 获取文件的扩展名(传入参数为string)
	  * @Description 
	  * @Title getFileExeName
	  * @Date 2021-1-16
	  * @author lixueyan
	  * @return String
	  * @param str
	  * @return
	  *
	  */
	 public static String getFileExeName(String str) {
			int indexOf = str.lastIndexOf(".");
			return str.substring(indexOf);
		}
	
	 /**
	  * 7.获取对应单位大小的文件
	  * @Description 
	  * @Title getFileSize
	  * @Date 2021-1-16
	  * @author lixueyan
	  * @return String
	  * @param file
	  * @param unit
	  * @return
	  *
	  */
	 public static String getFileSize(File file, FileSizeUnit unit) {
			// 获取文件的字节大小
			long length = file.length();

			BigDecimal size = BigDecimal.valueOf(length);
			BigDecimal code = BigDecimal.valueOf(unit.getCode());
			BigDecimal divide = size.divide(code, 2, RoundingMode.HALF_UP);
			// 根据传过来的单位，计算对应的数据
			return divide + "";
		}
	 /**
	  * 8.获取所在磁盘的总空间
	  * @Description 
	  * @Title getTotalSpace
	  * @Date 2021-1-16
	  * @author lixueyan
	  * @return long
	  * @param file
	  * @param unit
	  * @return
	  *
	  */
	 public static long getTotalSpace(File file, FileSizeUnit unit) {
			// 获取磁盘根目录
			File root = getFileRoot(file);

			// 获取总大小
			long totalSpace = root.getTotalSpace();

			// 根据传过来的单位，计算对应的数据
			return totalSpace / unit.getCode();
		}
	 
	 /**
	  * 9.刪除文件（递归）
	  * @Description 
	  * @Title deleteFiles
	  * @Date 2021-1-16
	  * @author lixueyan
	  * @return void
	  * @param file
	  *
	  */
	 public static void deleteFiles(File file) {

			// 获取文件目录结构
			File[] files = file.listFiles();
			// 判断
			if (files != null && files.length > 0) {
				for (File f : files) {
					if (f.isFile()) {
						f.delete();
					} else {
						deleteFiles(f);
					}
				}
			}
			file.delete();

		}

}
