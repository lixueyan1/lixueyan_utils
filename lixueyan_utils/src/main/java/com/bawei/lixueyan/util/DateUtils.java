package com.bawei.lixueyan.util;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.ZoneId;
import java.util.Calendar;
import java.util.Date;

/**
 * 时间处理工具类
 *  
 * @author lixueyan
 * @todo  TODO
 * @date  2021-1-13
 * @type_name  DateUtils
 */
public class DateUtils {
	/**
	 * 1.1将Date类型转换为String
	 * @Description 
	 * @Title getStrFromDate
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return String
	 * @param date
	 * @return
	 *
	 */
	public static String getStrFromDate(Date date) {
		// 转换类型固定：yyyy-MM-dd
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.format(date);
	}

	/**
	 * 1.2将Date类型转换为String，自定义日期格式类型
	 * @Description 
	 * @Title getStrFromDate
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return String
	 * @param date
	 * @param type
	 * @return
	 *
	 */
	public static String getStrFromDate(Date date, String type) {
		SimpleDateFormat format = new SimpleDateFormat(type);
		return format.format(date);
	}

	/**
	 * 	2.将string日期类型转换成date
	 * @Description 
	 * @Title getDateFromStr
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return Date
	 * @param date
	 * @return
	 * @throws Exception
	 *
	 */
	public static Date getDateFromStr(String date) throws Exception {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.parse(date);
	}

	/**
	 * 3.计算传入日期到现在共有多少月
	 * @Description 
	 * @Title getMonth
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return int
	 * @param date
	 * @return
	 *
	 */
	public static int getMonth(Date date) {
		// 1.获取当前系统时间（日历类）
		Calendar instance = Calendar.getInstance();
		// 获取当前年
		int nowYear = instance.get(Calendar.YEAR);
		// 获取当前月
		int nowMonth = instance.get(Calendar.MONTH);

		// 2.获取传入日期的年份跟月份
		instance.setTime(date);
		int impYear = instance.get(Calendar.YEAR);
		int impMonth = instance.get(Calendar.MONTH);

		// 3.跟据业务数据进行计算
		int result = 12 * (nowYear - impYear) + (nowMonth - impMonth);
		return result;
	}

	/**
	 * 4.计算传入日期到现在的年龄（根据生日计算年龄）
	 * @Description 
	 * @Title getAge
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return int
	 * @param birthday
	 * @return
	 *
	 */
	public static int getAge(Date birthday) {
		// 1.获取当前系统时间（日历类）
		Calendar instance = Calendar.getInstance();
		// 获取当前年
		int nowYear = instance.get(Calendar.YEAR);
		// 获取当前月
		int nowMonth = instance.get(Calendar.MONTH);
		// 获取当前日期(几号)
		int nowDay = instance.get(Calendar.DAY_OF_MONTH);

		// 2.获取传入日期的年份跟月份
		instance.setTime(birthday);
		int bYear = instance.get(Calendar.YEAR);
		int bMonth = instance.get(Calendar.MONTH);
		int bDay = instance.get(Calendar.DAY_OF_MONTH);

		// 3.跟据业务数据进行计算
		int age = 0;
		// 判断年份
		if (bYear > nowYear) {
			return -1; // 非法输入
		} else {
			age = nowYear - bYear;
		}
		// 判断月份
		if (bMonth > nowMonth) {
			age--;
		}
		// 判断年份
		if (bMonth == nowMonth && bDay > nowDay) {
			age--;
		}
		return age;
	}

	/**
	 * 	4.1 LocalDateTime实现年龄计算(计算传入日期到现在的年龄)
	 * @Description 
	 * @Title getAgeByLocal
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return int
	 * @param birthday
	 * @return
	 *
	 */
	public static int getAgeByLocal(Date birthday) {
		// 1.获取当前系统时间（日历类）
		LocalDateTime now = LocalDateTime.now();
		// 获取当前年
		int nowYear = now.getYear();
		// 获取当前月
		Month nowMonth = now.getMonth();
		// 获取当前日期(几号)
		int nowDay = now.getDayOfMonth();

		// 2.获取传入日期的年份跟月份（将我们的date类型转换成LocalDateTime）
		Instant instant = birthday.toInstant(); //
		// 获取系统默认时区
		ZoneId zoneId = ZoneId.systemDefault();
		LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();
		// 获取当前年
		int bYear = now.getYear();
		// 获取当前月
		Month bMonth = now.getMonth();
		// 获取当前日期(几号)
		int bDay = now.getDayOfMonth();

		// 3.跟据业务数据进行计算
		int age = 0;
		// 判断年份
		if (bYear > nowYear) {
			return -1; // 非法输入
		} else {
			age = nowYear - bYear;
		}
		// 判断月份;compareTo:-1 小,0 相等,1 大
		if (bMonth.compareTo(nowMonth) > 0) {
			age--;
		}
		// 判断年份
		if (bMonth.compareTo(nowMonth) == 0 && bDay > nowDay) {
			age--;
		}
		return age;
	}

	/**
	 * 	5.获取给定时间的月初开始时间（当月的第一天凌晨）
	 * @Description 
	 * @Title getFirstDayOfMonth
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return Date
	 * @param date
	 * @return
	 *
	 */
	public static Date getFirstDayOfMonth(Date date) {
		// 1.获取当前系统时间（日历类）
		Calendar instance = Calendar.getInstance();
		// 重新赋值时间
		instance.setTime(date);
		instance.set(Calendar.DAY_OF_MONTH, 1);
		instance.set(Calendar.HOUR_OF_DAY, 0);
		instance.set(Calendar.MINUTE, 0);
		instance.set(Calendar.SECOND, 0);
		return instance.getTime();
	}

	/**
	 * 6.获取给定时间的月末开始时间（当月的最后一天凌晨）
	 * @Description 
	 * @Title getLastDayOfMonth
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return Date
	 * @param date
	 * @return
	 *
	 */
	public static Date getLastDayOfMonth(Date date) {
		// 1.获取当前系统时间（日历类）
		Calendar instance = Calendar.getInstance();
		// 重新赋值时间
		instance.setTime(date);
		// 月份加一
		instance.add(Calendar.MONTH, 1);
		instance.set(Calendar.DAY_OF_MONTH, 1);
		instance.set(Calendar.HOUR_OF_DAY, 0);
		instance.set(Calendar.MINUTE, 0);
		instance.set(Calendar.SECOND, 0);
		// 秒减 1 
		instance.add(Calendar.SECOND, -1);
		return instance.getTime();
	}
	
	/**
	 * 7.重置当前时间；获取当前时间之前或者之后的某个时间点（正数：未来日期；负数：历史日期）
	 * @Description 
	 * @Title resetDate
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return Date
	 * @param days
	 * @return
	 *
	 */
	public static Date resetDate(int days) {
		// 1.获取当前系统时间（日历类）
		Calendar instance = Calendar.getInstance();
		instance.add(Calendar.DAY_OF_MONTH, days);
		return instance.getTime();
	}
	
	/**
	 * 	8.获取当前季节（春、夏、秋、冬）
	 * @Description 
	 * @Title getSeason
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return String
	 * @return
	 *
	 */
	public static String getSeason() {
		// 1.获取当前系统时间（日历类）
		Calendar instance = Calendar.getInstance();
		int month = instance.get(Calendar.MONTH) + 1;
		// 3/4/5春；6/7/8夏；9/10/11 秋；12/1/2 冬
		if (month < 9 && month >2) { // 春、夏
			if (month < 6) {
				return Season.SPRING.getName();
			}else {
				return Season.SUMMER.getName();
			}
		}else {
			if (month < 12 && month >= 9) {
				return Season.AUTUMN.getName();
			}
			return Season.WINTER.getName();
		}
	}
	
	/**
	 * 	9.获取人性化时间：例如5分钟之内则显示“刚刚”，其它显示16分钟前、2小时前、3天前、4月前、5年前等
	 * @Description 
	 * @Title getDisplayTime
	 * @Date 2021-1-16
	 * @author lixueyan
	 * @return String
	 * @param date
	 * @return
	 *
	 */
	public static String getDisplayTime(Date date) {
		// 获取传入日期的毫米值 ;从1970-1-1 到date 的毫米值
        long t1 = date.getTime();
        // 当前系统日期的毫米值;即 从1970-1-1 到现在的毫米值
        long t2 = new Date().getTime();
        // 获取两个时间的差值
        long t = t2 - t1;
        // 分钟
        long minute = t / 1000 / 60;
        if (t2 < t1)
             return "未来时间";
        if (minute / 60 / 24 / 30 / 12 >= 1)
             return minute / 60 / 24 / 30 / 12 + "年前";
        else if (minute / 60 / 24 / 30 >= 1)
             return minute / 60 / 24 / 30 + "月前";
        else if (minute / 60 / 24 >= 1)
             return minute / 60 / 24 + "天前";
        else if (minute / 60 >= 1)
             return minute / 60 + "小时前";
        else if (minute > 5)//
             return minute + "分钟前";
        else
             return "刚刚";
	}



}
