package com.bawei.lixueyan.util;

public enum FileSizeUnit {
	 B(1, "B"),
     K(1024, "KB"),
     M(1024 * 1024, "MB"),
     G(1024 * 1024 * 1024, "GB"),
     T(1024 * 1024 * 1024 * 1024, "TB"),
     P(1024 * 1024 * 1024 * 1024 * 1024, "PB");
	
	private long code;
	
	private String vale;

	public long getCode() {
		return code;
	}

	public String getVale() {
		return vale;
	}

	private FileSizeUnit(long code, String vale) {
		this.code = code;
		this.vale = vale;
	}

}
