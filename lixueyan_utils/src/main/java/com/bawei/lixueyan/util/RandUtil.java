package com.bawei.lixueyan.util;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;
import java.util.UUID;

/**
 * 随机数工具类
 *  
 * @author lixueyan
 * @todo  TODO
 * @date  2021-1-14
 * @type_name  RandUtil
 */
public class RandUtil {
	/**
	 * 1. 获取随机正整数
	 * @Description 
	 * @Title getIntNext
	 * @Date 2021-1-14
	 * @author lixueyan
	 * @return int
	 * @return
	 *
	 */
	public static int getIntNext() {
		//创建随机数类
		Random random = new Random();
		//获取随机数值
		int nextInt = random.nextInt();
		System.out.println("原值:"+nextInt);
		return Math.abs(nextInt);
	}
	/**
	 * 2. 获取指定最大范围    的随机正整数
	 * @Description 
	 * @Title getIntNext
	 * @Date 2021-1-14
	 * @author lixueyan
	 * @return int
	 * @param max
	 * @return
	 *
	 */
	public static int getIntNext(int max) {
		//创建随机数类
		Random random = new Random();
		//获取随机数值(0,max)
		return random.nextInt(max);
	}
	
	/***
	 * 3. 获取指定范围的随机正整数
	 * @Description 
	 * @Title getIntNext
	 * @Date 2021-1-14
	 * @author lixueyan
	 * @return int
	 * @param min
	 * @param max
	 * @return
	 *
	 */
	public static int getIntNext(int min,int max) {
		//创建随机数类
		Random random = new Random();
		//获取随机数值(5,10)
		return random.nextInt(max-min + 1) + min;
	}
	/**
	 * 4.获取最小值和最大值之间截取指定长度的随机数
	 * @Description 
	 * @Title getIntNext
	 * @Date 2021-1-14
	 * @author lixueyan
	 * @return int
	 * @param min
	 * @param max
	 * @param length
	 * @return
	 *
	 */
	public static String getIntNext(int min, int max, int length) {
		// 创建随机数类
		Random random = new Random();
		// 获取随机数值(5,10)
		StringBuffer strNum = new StringBuffer();
		String str = "";
		for (int i = 0; i < length; i++) {
			int next = getIntNext(min, max);
			str += next;
		}
		return str;
	}
		
	
	/**
	 * 5. 获取1-9,a-Z之间的随机字符，即包括数字或字母(含大小写)的字符
	 * @Description 
	 * @Title getCharFormStr
	 * @Date 2021-1-14
	 * @author lixueyan
	 * @return char
	 * @return
	 *
	 */
	public static char getCharFormStr() {
		//获取0-9,a-Z字符串
        String str =  "1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        //取str的下标
        int index = getIntNext(str.length()-1);
        return str.charAt(index);
        
	}
	/**
	 *  6.生成指定长度的字符串，（1-9,a-Z）
	 * @Description 
	 * @Title nextString
	 * @Date 2021-1-14
	 * @author lixueyan
	 * @return String
	 * @param length
	 * @return
	 *
	 */
	public static String getStrFromRandom(int length) {
		// 创建存储字符的容器
		String result = "";
		// 遍历
		for (int i = 0; i < length; i++) {
			result += getCharFormStr();
		}
		return result;
	}
	
	/**
	 * 7. 获取一个随机中文字符
	 * @Description 
	 * @Title nextSimplifiedChineseCharacter
	 * @Date 2021-1-14
	 * @author lixueyan
	 * @return String
	 * @return
	 * @throws UnsupportedEncodingException
	 *
	 */
	public static String getChineseCharacter() throws UnsupportedEncodingException {
		// 根据区位码进行计算

		// 设置区码
		// 16区-87区 1区的区位码是A1，换成十进制是161
		// 16区的区码是B0 换成十进制为176
		byte high_code = (byte) (160 + getIntNext(16, 87));

		// 设置位码
		// 从A0-FF 不包含A0和FF A0 160; FF 255
		byte low_code = (byte) getIntNext(161, 254);

		// 处理55区的数据，55区只有89个汉字，缺5个
		if (high_code == 215) {
			// 重新获取位码值
			low_code = (byte) getIntNext(161, 249);
		}

		// 将区位码存入字节数组
		byte[] bytes = new byte[] { high_code, low_code };

		// 将数组转换成String字符串
		String str = new String(bytes, "GB2312");
		return str;
	}
	 /**
	  * 8. 获取不带横线的UUID（上传文件时作为新文件名）
	  * @Description 
	  * @Title uuid
	  * @Date 2021-1-14
	  * @author lixueyan
	  * @return String
	  * @return
	  *
	  */
	public static String getUUID() {
		// 使用UUID工具类生成id信息
		String randomUUID = UUID.randomUUID().toString();
		System.out.println("原始数据：" + randomUUID);
		// 数据处理
		String resultId = randomUUID.replaceAll("-", "");
		return resultId;
	}
	 /**
	  * 生成指定范围内随机日期
	  * @Description 
	  * @Title randomDate
	  * @Date 2021-1-14
	  * @author lixueyan
	  * @return Date
	  * @param d1
	  * @param d2
	  * @return
	  *
	  */
	// 传入参数是日期类型
	public static Date getRandomDate(Date d1, Date d2) {
		// 将日期类型转换为long
		long time_1 = d1.getTime();
		long time_2 = d2.getTime();
		
		// 数据计算
		long l3= (long) ((Math.random() * (time_2-time_1 +1))  + time_1);
		return new Date(l3);
	}
	    /**
	     * 生成指定范围内随机日期
	     * @Description 
	     * @Title randomDate
	     * @Date 2021-1-14
	     * @author lixueyan
	     * @return Date
	     * @param stratDate
	     * @param endDate
	     * @return
	     *
	     */
	 // 传入参数是String类型
	public static Date getRandomDate(String d1, String d2) throws Exception {
		// 数据转换
		Date date_1 = DateUtils.getDateFromStr(d1);
		Date date_2 = DateUtils.getDateFromStr(d2);
		
		// 将日期类型转换为long
		long time_1 =  date_1.getTime();
		long time_2 =  date_2.getTime();
		
		// 数据计算
		long l3= (long) ((Math.random() * (time_2-time_1 +1))  + time_1);
		return new Date(l3);
	}
	    
	public static String getChinesePersonName() throws  Exception{
        //获取姓氏
        String [] firstNames= new String[] {
                  "赵", "钱", "孙", "李", "周", "吴",  "郑", "王", "冯", "陈", "褚", "卫", "蒋", "沈", "韩", "杨", "朱", "秦",
                 "尤", "许", "何", "吕", "施", "张",  "孔", "曹", "严", "华", "金", "魏", "陶", "姜", "戚", "谢", "邹", "喻", "柏", "水", "窦",
                 "章", "云", "苏", "潘", "葛", "奚",  "范", "彭", "郎", "鲁", "韦", "昌", "马", "苗", "凤", "花", "方", "俞", "任", "袁", "柳",
                 "酆", "鲍", "史", "唐", "费", "廉",  "岑", "薛", "雷", "贺", "倪", "汤", "滕", "殷", "罗", "毕", "郝", "邬", "安", "常", "乐",
                 "于", "时", "傅", "皮", "卞", "齐",  "康", "伍", "余", "元", "卜", "顾", "孟", "平", "黄", "和", "穆", "萧", "尹", "姚", "邵",
                 "湛", "汪", "祁", "毛", "禹", "狄",  "米", "贝", "明", "臧", "计", "伏", "成", "戴", "谈", "宋", "茅", "庞", "熊", "纪", "舒",
                 "屈", "项", "祝", "董", "梁", "杜",  "阮", "蓝", "闵", "席", "季", "麻", "强", "贾", "路", "娄", "危", "江", "童", "颜", "郭",
                 "梅", "盛", "林", "刁", "钟", "徐",  "邱", "骆", "高", "夏", "蔡", "田", "樊", "胡", "凌", "霍", "虞", "万", "支", "柯", "昝",
                 "管", "卢", "莫", "经", "房", "裘",  "缪", "干", "解", "应", "宗", "丁", "宣", "贲", "邓", "郁", "单", "杭", "洪", "包", "诸",
                 "左", "石", "崔", "吉", "钮", "龚",  "程", "嵇", "邢", "滑", "裴", "陆", "荣", "翁", "荀", "羊", "于", "惠", "甄", "曲", "家",
                 "封", "芮", "羿", "储", "靳", "汲",  "邴", "糜", "松", "井", "段", "富", "巫", "乌", "焦", "巴", "弓", "牧", "隗", "山", "谷",
                 "车", "侯", "宓", "蓬", "全", "郗",  "班", "仰", "秋", "仲", "伊", "宫", "宁", "仇", "栾", "暴", "甘", "钭", "厉", "戎", "祖",
                 "武", "符", "刘", "景", "詹", "束",  "龙", "叶", "幸", "司", "韶", "郜", "黎", "蓟", "溥", "印", "宿", "白", "怀", "蒲", "邰",
                 "从", "鄂", "索", "咸", "籍", "赖",  "卓", "蔺", "屠", "蒙", "池", "乔", "阴", "郁", "胥", "能", "苍", "双", "闻", "莘", "党",
                 "翟", "谭", "贡", "劳", "逄", "姬",  "申", "扶", "堵", "冉", "宰", "郦", "雍", "却", "璩", "桑", "桂", "濮", "牛", "寿", "通",
                 "边", "扈", "燕", "冀", "浦", "尚",  "农", "温", "别", "庄", "晏", "柴", "瞿", "阎", "充", "慕", "连", "茹", "习", "宦", "艾",
                 "鱼", "容", "向", "古", "易", "慎",  "戈", "廖", "庾", "终", "暨", "居", "衡", "步", "都", "耿", "满", "弘", "匡", "国", "文",
                 "寇", "广", "禄", "阙", "东", "欧",  "殳", "沃", "利", "蔚", "越", "夔", "隆", "师", "巩", "厍", "聂", "晁", "勾", "敖", "融",
                 "冷", "訾", "辛", "阚", "那", "简",  "饶", "空", "曾", "毋", "沙", "乜", "养", "鞠", "须", "丰", "巢", "关", "蒯", "相", "查",
                 "后", "荆", "红", "游", "郏", "竺",  "权", "逯", "盖", "益", "桓", "公", "仉", "督", "岳", "帅", "缑", "亢", "况", "郈", "有",
                 "琴", "归", "海", "晋", "楚", "闫",  "法", "汝", "鄢", "涂", "钦", "商", "牟", "佘", "佴", "伯", "赏", "墨", "哈", "谯", "篁",
                 "年", "爱", "阳", "佟", "言", "福",  "南", "火", "铁", "迟", "漆", "官", "冼", "真", "展", "繁", "檀", "祭", "密", "敬", "揭",
                 "舜", "楼", "疏", "冒", "浑", "挚",  "胶", "随", "高", "皋", "原", "种", "练", "弥", "仓", "眭", "蹇", "覃", "阿", "门", "恽",
                 "来", "綦", "召", "仪", "风", "介", "巨", "木", "京", "狐", "郇", "虎", "枚",  "抗", "达", "杞", "苌", "折", "麦", "庆", "过",
                 "竹", "端", "鲜", "皇", "亓", "老", "是", "秘", "畅", "邝", "还", "宾", "闾", "辜", "纵", "侴", "万俟", "司马", "上官", "欧阳",
                 "夏侯", "诸葛", "闻人", "东方", "赫连", "皇甫", "羊舌", "尉迟", "公羊", "澹台", "公冶", "宗正", "濮阳", "淳于", "单于", "太叔", "申屠",
                 "公孙", "仲孙", "轩辕", "令狐", "钟离", "宇文", "长孙", "慕容", "鲜于", "闾丘", "司徒", "司空", "兀官", "司寇", "南门", "呼延", "子车",
                 "颛孙", "端木", "巫马", "公西", "漆雕", "车正", "壤驷", "公良", "拓跋", "夹谷", "宰父", "谷梁", "段干", "百里", "东郭", "微生", "梁丘",
                 "左丘", "东门", "西门", "南宫", "第五", "公仪", "公乘", "太史", "仲长", "叔孙", "屈突", "尔朱", "东乡", "相里", "胡母", "司城",  "张廖",
                 "雍门", "毋丘", "贺兰", "綦毋", "屋庐",  "独孤", "南郭", "北宫", "王孙" };
        
        //随机下标
        int nextInt = getIntNext(firstNames.length - 1);
        
        //获取姓氏
        String name = firstNames[nextInt];
        
        //获取名字，随机一个或两个汉字
        int num = getIntNext(1, 2);
        
        //根据随机出来的数据，循环追加名字
        for (int i = 0; i < num; i++) {
             name += getChineseCharacter();
        }
        return name;
    }
	
}
