package com.bawei.lixueyan.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtil {
	
	 // 数字正则表达式
    private static String NUMBER_REG = "^\\d+$";
    // 实数正则表达式（是有理数和无理数的总称）
    private static String REAL_REG =  "^(-)?[0-9]+(\\.[0-9]+)?$";
    
    //判断字符串是否全为数字
    /**
     * 1. 判断字符串是否全为数字(有理数和无理数)
     * @Description 
     * @Title isNumber
     * @Date 2021-1-14
     * @author lixueyan
     * @return boolean
     * @param str
     * @return
     *
     */
    public static boolean isNumber(String str) {
    	if(StringUtil.hasText(str)) {
    		return false;
    	}
    	return str.matches(NUMBER_REG);
    }
    /**
     * 2. 判断传入的字符串是否全为实数
     * @Description 
     * @Title isReal
     * @Date 2021-1-14
     * @author lixueyan
     * @return boolean
     * @param str
     * @return
     *
     */
    public static boolean isReal(String str) {
    	if(StringUtil.hasText(str)) {
    		return false;
    	}
    	return str.matches(REAL_REG);
    }
    /**
     * 3. 提供精确加法运算
     * @Description 
     * @Title add
     * @Date 2021-1-14
     * @author lixueyan
     * @return double
     * @param v1
     * @param v2
     * @return
     *
     */
    public static double add(double v1,double v2) {
    	// 换成精准类型
        BigDecimal bd1 = new BigDecimal(v1 + "");
        BigDecimal bd2 = new BigDecimal(v2 + "");
        return bd1.add(bd2).doubleValue();
    }
    /**
     * 4. 提供精确减法运算
     * @Description 
     * @Title sub
     * @Date 2021-1-14
     * @author lixueyan
     * @return double
     * @param v1
     * @param v2
     * @return
     *
     */
    public static double sub(double v1, double v2) {
        // 换成精准类型
        BigDecimal bd1 = new BigDecimal(v1 + "");
        BigDecimal bd2 = new BigDecimal(v2 + "");
        return bd1.subtract(bd2).doubleValue();
    }
    /**
     * 5. 精准的乘法运算
     * @Description 
     * @Title mul
     * @Date 2021-1-14
     * @author lixueyan
     * @return double
     * @param v1
     * @param v2
     * @return
     *
     */
    public static double mul(double v1, double v2) {
        // 换成精准类型
        BigDecimal bd1 = new BigDecimal(v1 + "");
        BigDecimal bd2 = new BigDecimal(v2 + "");
        return bd1.multiply(bd2).doubleValue();
    }
    /**
     * 6. 精准的除法运算
     * @Description 
     * @Title div
     * @Date 2021-1-14
     * @author lixueyan
     * @return double
     * @param v1
     * @param v2
     * @return
     *
     */
    public static double div(double v1, double v2) {
        // 换成精准类型
        BigDecimal bd1 = new BigDecimal(v1 + "");
        BigDecimal bd2 = new BigDecimal(v2 + "");
        return bd1.divide(bd2).doubleValue();
    }
    /**
     * 7. 精准的四舍五入方法
     * @Description 
     * @Title round
     * @Date 2021-1-14
     * @author lixueyan
     * @return double
     * @param v
     * @param scale
     * @return
     *
     */
    public static double round(double v,int scale) {
    	BigDecimal bd = new BigDecimal(v + "");
    	return bd.setScale(scale,RoundingMode.HALF_UP).doubleValue();
    }

}
