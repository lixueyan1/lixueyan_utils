package com.bawei.lixueyan.util;

public enum Season {
	SPRING("0","春"),
	SUMMER("1","夏"),
	AUTUMN("2","秋"),
	WINTER("3","冬");
	
	private String code;
	
	private String name;

	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}

	private Season(String code, String name) {
		this.code = code;
		this.name = name;
	}

}
