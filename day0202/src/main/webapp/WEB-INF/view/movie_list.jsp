<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath }/resource/css/bootstrap.css">
<script type="text/javascript" src="${pageContext.request.contextPath }/resource/js/jquery-3.2.1.js"></script>
</head>
<body>
<form id="sreachFor" action="${pageContext.request.contextPath }/list" method="post">
	
	<input type="hidden" value="${pageInfo.pageNum }" name="pageNum" id="pageNum">
	<div id = "di"></div>
</form>
<input type="button" value="查询电影" onclick="cx()">
<input type="button" value="删除电影" onclick="de()">
	<table>
		<tr>
			<td><input type="checkbox" id="ck" onclick="qfx(this)"></td>
			<td>ID</td>
			<td>名称</td>
			<td>导演</td>
			<td>票价</td>
			<td>上映时间</td>
			<td>时长</td>
			<td>类型</td>
			<td>年代</td>
			<td>区域</td>
			<td>状态</td>
			<td>操作</td>
		</tr>
		<c:forEach items="${pageInfo.list }" var="i">
			<tr>
				<td><input type="checkbox" class="ck" value="${i.id }"></td>
				<td>${i.id }</td>
				<td>${i.name }</td>
				<td>${i.director }</td>
				<td>${i.price }</td>
				
				<td>${i.releasee }</td>
				<td>${i.duration }</td>
				<td>${i.typee }</td>
				<td>${i.years }</td>
				<td>${i.region }</td>
				<td>${i.state==0?'正在热映':'已经下架' }</td>
				<td>
				<a href="#">详情</a>
				<a href="#">编辑</a>
				<c:if test="${i.state==0 }">
				<a href="statex?id=${i.id }">下架</a>
				</c:if>
				<c:if test="${i.state==1 }">
				<a href="states?id=${i.id }">上架</a>
				</c:if>
				
				</td>
			</tr>
		</c:forEach>
	</table>
	<jsp:include page="/WEB-INF/page/page.jsp"></jsp:include>
	<script type="text/javascript">
		/* 分页提交表单 */
		function searchData(pageNum){
			$("#pageNum").val(pageNum);
			$("#sreachFor").submit();
		}
		
		function qfx(inputDom){
			$(".ck").attr("checked",inputDom.checked)
		}
		
		function cx(){
			$("#di").append('电影名称:<input type="text" value="${movie.name }" name="name">'+
					'上映时间: <input type="date" name="start" value="${movie.start }">至'+
					'<input type="date" name="end" value="${movie.end }"> <br>'+
					'导演:<input type="text" value="${movie.director }" name="director">'+
					'价格: <input type="text" name="low" value="${movie.low }">-'+
					'<input type="text" name="high" value="${movie.high }"> <br>'+
					'电影年代:<input type="text" value="${movie.years }" name="years">'+
					'电影时长: <input type="text" name="shortt" value="${movie.shortt }">-'+
					'<input type="text" name="longg" value="${movie.longg }"> <br>'+
					'<button>搜索</button><input type="reset" value="重置">')
		}
		
		//获取id
		
		function de(){
			var id = $(".ck:checked").map(function(){
				return this.value;
			}).get().join(",");
			if(confirm("确认删除"+id+"吗?")){
			"delete",
			{id:id},
			function(msg){
				if(msg){
					alert("删除成功");
					location = "list";
				}else{
					alert("删除失败");
				}
			},"join"
			
			}
			
		}
	
	</script>
</body>
</html>