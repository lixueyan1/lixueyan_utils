<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div>
	<!-- 判断是否有上一页 -->
	<c:if test="${pageInfo.pageNum>1}">
		<a class="sel_btn"
			href="javascript:searchData(${pageInfo.prePage==0?1:pageInfo.prePage})">上一页</a>
	</c:if>
	<!-- 遍历导航页码 -->
	<c:forEach var="page" items="${pageInfo.navigatepageNums}">
		<c:if test="${pageInfo.pageNum==page }">
			<a class="sel_btn ch_cls" href="javascript:searchData(${page})">${page }</a>
		</c:if>
		<c:if test="${pageInfo.pageNum!=page }">
			<a class="sel_btn" href="javascript:searchData(${page})">${page }</a>
		</c:if>
	</c:forEach>

	<!-- 判断是否有下一页 -->
	<c:if test="${pageInfo.pageNum!=pageInfo.pages }">
		<a class="sel_btn"
			href="javascript:searchData(${pageInfo.nextPage==0?pageInfo.pages:pageInfo.nextPage})">下一页</a>
	</c:if>
</div>