package com.bawei.lixueyan.service;

import com.bawei.lixueyan.entity.Movie;
import com.github.pagehelper.PageInfo;

public interface MovieService {
	
	PageInfo<Movie> select(Integer pageNum,Integer pageSize,Movie movie);
	/**
	 * 修改电影下架
	 * @param id
	 */
	void state0(Integer id);
	/**
	 * 根据id查询
	 * @param id
	 * @return
	 */
	Movie selectById(Integer id);
	/**
	 * 修改电影上架
	 * @param id
	 */
	void state1(Integer id);
	//批删
	void delete(Integer[] id);
	//添加
	void insert(Movie movie);

}
