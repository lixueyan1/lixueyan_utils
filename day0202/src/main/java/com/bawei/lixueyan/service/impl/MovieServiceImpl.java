package com.bawei.lixueyan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bawei.lixueyan.dao.MovieMapper;
import com.bawei.lixueyan.entity.Movie;
import com.bawei.lixueyan.service.MovieService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
@Service
public class MovieServiceImpl implements MovieService{
	@Autowired
	private MovieMapper movieMapper;

	@Override
	public PageInfo<Movie> select(Integer pageNum, Integer pageSize, Movie movie) {
		// TODO Auto-generated method stub
		PageHelper.startPage(pageNum, pageSize);
		List<Movie> list = movieMapper.select(movie);
		return new PageInfo<Movie>(list);
	}

	@Override
	public void state0(Integer id) {
		// TODO Auto-generated method stub
		movieMapper.state0(id);
		
	}

	@Override
	public Movie selectById(Integer id) {
		// TODO Auto-generated method stub
		Movie movie = movieMapper.selectById(id);
		return movie;
	}

	@Override
	public void state1(Integer id) {
		// TODO Auto-generated method stub
		movieMapper.state1(id);
		
	}

	@Override
	public void delete(Integer[] id) {
		// TODO Auto-generated method stub
		int i = movieMapper.delete(id);
	}

	@Override
	public void insert(Movie movie) {
		// TODO Auto-generated method stub
		movieMapper.insert(movie);
		
	}

}
