package com.bawei.lixueyan.entity;

import java.io.Serializable;
/**
 * 
 *  
 * @author lixueyan
 * @todo  实体类
 * @date  2021-1-7
 * @type_name  Movie
 */
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;
public class Movie implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5491184591158002653L;
	
	
	private Integer id;
	private String name;
	private String director;
	private double price;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date  releasee;
	private Integer duration;
	private String typee;
	private String years;
	private String region;
	private Integer state;
	
	
	
	private String start;
	private String end;
	private String low;
	private String high;
	private String shortt;
	private String longg;
	
	
	
	
	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getEnd() {
		return end;
	}

	public void setEnd(String end) {
		this.end = end;
	}

	public String getLow() {
		return low;
	}

	public void setLow(String low) {
		this.low = low;
	}

	public String getHigh() {
		return high;
	}

	public void setHigh(String high) {
		this.high = high;
	}

	public String getShortt() {
		return shortt;
	}

	public void setShortt(String shortt) {
		this.shortt = shortt;
	}

	public String getLongg() {
		return longg;
	}

	public void setLongg(String longg) {
		this.longg = longg;
	}

	public Movie() {
		// TODO Auto-generated constructor stub
	}

	public Movie(Integer id, String name, String director, double price, Date releasee, Integer duration, String typee,
			String years, String region, Integer state) {
		super();
		this.id = id;
		this.name = name;
		this.director = director;
		this.price = price;
		this.releasee = releasee;
		this.duration = duration;
		this.typee = typee;
		this.years = years;
		this.region = region;
		this.state = state;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getReleasee() {
		return releasee;
	}

	public void setReleasee(Date releasee) {
		this.releasee = releasee;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getTypee() {
		return typee;
	}

	public void setTypee(String typee) {
		this.typee = typee;
	}

	public String getYears() {
		return years;
	}

	public void setYears(String years) {
		this.years = years;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	@Override
	public String toString() {
		return "Movie [id=" + id + ", name=" + name + ", director=" + director + ", price=" + price + ", releasee="
				+ releasee + ", duration=" + duration + ", typee=" + typee + ", years=" + years + ", region=" + region
				+ ", state=" + state + "]";
	}
	
	
	
	

}
