package com.bawei.lixueyan.dao;

import java.util.List;

import com.bawei.lixueyan.entity.Movie;

public interface MovieMapper {
	
	List<Movie> select(Movie movie);

	Movie selectById(Integer id);
	/**
	 * 下架
	 * @param id
	 * @return
	 */
	int  state0(Integer id);
	/**
	 * 上架
	 * @param id
	 * @return
	 */
	int  state1(Integer id);
/**
 * 批删
 * @param id
 * @return
 */
	int delete(Integer[] id);
	/**
	 * 添加
	 * @param movie
	 * @return
	 */
	int insert(Movie movie);

}
