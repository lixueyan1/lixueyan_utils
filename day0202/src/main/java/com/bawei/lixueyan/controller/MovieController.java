package com.bawei.lixueyan.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.bawei.lixueyan.entity.Movie;
import com.bawei.lixueyan.service.MovieService;
import com.github.pagehelper.PageInfo;
import com.sun.org.apache.regexp.internal.recompile;

@Controller
public class MovieController {
	@Autowired
	private MovieService movieService;
	
	@RequestMapping("list")
	public String select(HttpServletRequest request,@RequestParam(defaultValue = "1")Integer pageNum,@RequestParam(defaultValue = "3")Integer pageSize,Movie movie) {
		PageInfo<Movie> pageInfo = movieService.select(pageNum, pageSize, movie);
		System.out.println(pageInfo.getList());
		request.setAttribute("pageInfo", pageInfo);
		request.setAttribute("movie", movie);
		return "movie_list";
		
	}
	
	@RequestMapping("statex")
	public String statee1(Integer id) {
		//Movie movie = movieService.selectById(id);
		movieService.state0(id);
		
		return "redirect:list";
	}
	@RequestMapping("states")
	public String statee0(Integer id) {
	//	Movie movie = movieService.selectById(id);
		movieService.state1(id);
		
		return "redirect:list";
	}
	
	@RequestMapping("delete")
	public String deleteMovie(Integer[] id) {
		movieService.delete(id);
		return "redirect:list";
	}
	
	@RequestMapping("toAdd")
	public String add() {
		return "movie_insert";
	}
	@RequestMapping("insert")
	public String insertMovie(Movie movie) {
		movieService.insert(movie);
		return "redirect:list";
	}

}
