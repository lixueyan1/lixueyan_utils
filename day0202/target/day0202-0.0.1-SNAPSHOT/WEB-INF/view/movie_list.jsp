<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>    
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
<link rel="stylesheet" href="${pageContext.request.contextPath }/css/index.css">
<script type="text/javascript" src="${pageContext.request.contextPath }/js/jquery-3.2.1.js"></script>
</head>
<body>
<form id="sreachFor" action="${pageContext.request.contextPath }/list">
	姓名:<input type="text" value="${user.name }" name="name">
	<input type="hidden" value="${pageInfo.pageNum }" name="pageNum" id="pageNum">
	<button>搜索</button>
</form>
	<table>
		<tr>
			<td><input type="checkbox" id="ck" onclick="qfx(this)"></td>
			<td>ID</td>
			<td>名称</td>
			<td>导演</td>
			<td>票价</td>
			<td>上映时间</td>
			<td>时长</td>
			<td>类型</td>
			<td>年代</td>
			<td>区域</td>
			<td>状态</td>
			<td>操作</td>
		</tr>
		<c:forEach items="${pageInfo.list }" var="i">
			<tr>
				<td><input type="checkbox" class="ck"></td>
				<td>${i.id }</td>
				<td>${i.name }</td>
				<td>${i.director }</td>
				<td>${i.price }</td>
				
				<td>${i.releasee }</td>
				<td>${i.duration }</td>
				<td>${i.type }</td>
				<td>${i.years }</td>
				<td>${i.region }</td>
				<td>${i.state==0?'正在热映':'已经下架' }</td>
				<td>
				<a href="#">详情</a>
				<a href="#">编辑</a>
				<c:if test="${i.state==0 }">
				<a href="#">下架</a>
				</c:if>
				<c:if test="${i.state==1 }">
				<a href="#">上架</a>
				</c:if>
				
				</td>
			</tr>
		</c:forEach>
		<jsp:include page="/WEB-INF/pages/page.jsp"></jsp:include>
	</table>
	<script type="text/javascript">
		/* 分页提交表单 */
		function sreadDom(pageNum){
			$("#pageNum").val(pageNum);
			$("#sreachFor").submit();
		}
		
		function qfx(inputDom){
			$(".ck").attr("checked",inputDom.checked)
		}
	
	</script>
</body>
</html>